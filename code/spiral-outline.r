#Assigning the planar triangle function and creating a filter that reduces its dimension
spiral <- sample_arch_spiral(110, max_wrap = 1)
f_x <- matrix(spiral[ ,2])

#Running the Mapper Construction with the planar triangle 
m <- MapperRef$new(X = spiral)$
  use_filter(filter= f_x)$
  use_cover(cover = "restrained interval", number_intervals = 10, percent_overlap = 80)$
  use_clustering_algorithm(cl = "single")$
  construct_k_skeleton(k = 1L)
#print(m)

#Plotting the Construction
#plot(m$simplicial_complex)
plot(m$as_igraph())

