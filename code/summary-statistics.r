library(dplyr)
library(tibble)

summary_statistics <- function() {
  d <- tibble()
  g <- tibble()
  g_i <- tibble()
  num_intervals <- 3:30
  percent_overlap <- seq(20, 80, 10)
  trials <- 1:10
  
  for (interval in num_intervals) for (percent in percent_overlap) for(trial_num in trials ) {
    trefoil <- sample_trefoil(240,0)
    f_x <- matrix(trefoil[ ,3])
    #Running the Mapper Construction with the figure eight
    m <- MapperRef$new(X = trefoil)$
      use_filter(filter= f_x)$
      use_cover(cover = "restrained interval", number_intervals = interval, percent_overlap = percent)$
      use_clustering_algorithm(cl = "single")$
      construct_k_skeleton(k = 1L)
    chi <- calc_euler_num(m)
    if(chi == 0){
      loop <- "true"
      g_i <- tibble(interval = interval,
                    percent = percent)
    } else {
      loop <- "false"
    }
    d_i <- tibble(trial_num = trial_num,
                  interval = interval,
                  percent = percent,
                  chi = chi, 
                  loop = loop)
    d <- bind_rows(d, d_i)
    g <- bind_rows(g, g_i)
  }
  print(d)
  print(g)
}

summary_statistics()

