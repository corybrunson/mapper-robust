calc_euler_num <- function(construction) {
  d <- length(construction$simplicial_complex$as_list())#number of dimensions
  k <- 0
  sum <- 0
  while (k < d) {
    k_simplex <- nrow(construction$simplicial_complex$as_list()[[k + 1]])
    sum = sum + (k_simplex * ((-1) ^ k))
    k = k + 1
  }
  sum
}
