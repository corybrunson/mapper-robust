
library(tidyverse)

# Reaven & Miller (1979) - diabetes data set - heplots package
diabetes <- as_tibble(heplots::Diabetes)

# Nielson &al (2017) - traumatic brain injury dataset - PLoS One
# download S1 Dataset and S1 Metadata to `data-raw` folder:
# https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0169490
tbi <- readxl::read_xlsx("data-raw/journal.pone.0169490.s010.xlsx")
tbi_score <- select(tbi, 2:3)
tbi_tda <- select(tbi, 4:20)
tbi_snp <- select(tbi, starts_with("rs"))
