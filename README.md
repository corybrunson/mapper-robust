# Robustness Analysis of the Mapper Construction

## Exploratory Phase

**Motivation**

The overarching goal of this study is to determine over what range of settings the Mapper construction is able to successfully recover known topological features using toy data sets of known topology from the R package tdaunif.

---

**Design**

1. Choose the shapes, number of observations(n), and the amplitude of noise of the toy data sets being used.
	- Shapes: Figure Eight (n = 360), Spiral (n = 900), Trefoil Knot (n = 720)
	- Standard Deviation of Noise: 0
2. Choose the parameters of the Mapper construction that will be kept constant.
	- Filter: Project the points onto 1-dimensional space by collapsing the x-coordinates.
	- Clustering Method: Single Linkage with a Euclidean metric
3. Determine what parameters of the Mapper construction will be manipulated.
	- Cover:
		- Cover Types: Fixed, Restrained, Ball, Dual Tessellations
		- Cover Resolution: Number of Intervals, Percent Overlap or Epsilon
4. Record findings in a spreadsheet, specifying the parameters used and whether the structure of the shape is recovered.

---

**Calculations**


