Mean Distances should be less than 0.01

|  Sampler       | Observations |
| ---------------|--------------|
| trefoil        | 240          |
| figure_eight   | 155          |
| planar_triangle| 1250         |
| spiral         | 110     		  |
| torus          | 5500    		  |
