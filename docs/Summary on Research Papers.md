Network Tomography for Understanding Phenotypic Presentations in Aortic Stenosis (2019)

The study conducted by Casaclang-Verzosa had a primary objective of building similarity network models for patients with Aortic Stenosis (AS). AS occurs when the heart’s valve narrows which prevents the valve from opening fulling and thus reduces blood flow from your heart to your body. The network model was developed from topological data analysis of echocardiographic data from 246 patients and the severity of AS was identified for each patient. The cross sectional echocardiography data from humans developed a compressed disease map. The same network model was applied to a murine model to determine if the same map was recaptured. The resulting network defines the shape of the high dimensional data where each node represents groups of patients who are similar to each other across multiple features and each edge connects nodes that contain similar patients. The differences in phenotypes were graded by location and color across the map, where red represented severe and blue represented mild/normal. The model produced two connected pathways in which mild forms of AS could progress to clinically severe forms of AS. This model suggests that it might be possible to identify a subset of patients who will experience aggressive deterioration of the heart valve at a much earlier point. 

Topological Data Analysis was performed using Cloud based analytic platform. The metric used was normalized correlation metric, which measures similarity between two points.

| Data  | Resolution | Gain  |         
| ------|------------|-------|
| Human | 24         | 2.5   |   
| Mice  | PCA 1 - 27 | 2.8   |
| Mice  | PCA 2 - 38 | 3.0   |
