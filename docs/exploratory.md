# Exploratory Study of the Mapper Package

In order to determine the range of parameters for which the Mapper Construction successfully recovers known topological features, point data clouds were produced by uniform samplers of three shapes with known topological structure were used for this study: the trefoil knot, the archimedean spiral, the planar triangle and the figure eight. There was zero noise applied to these samples to ensure that the parameter range identified is as wide as possible, as naturally the greater the amount of noise, the less consistent Mapper is able to recover the shapes topological features due to random chance. The number of observations in the samples varies from shape to shape, and were chosen so that the minimum distances (from a sampled point to another sampled point) divided by the diameter of the dataset is less than 0.01. In this exploratory phase, the filter type (projecting the points onto 1-dimensional space by collapsing the x-coordinates) and clustering algorithm (single-linkage) were both kept constant. The mapper construction was performed with the Fixed Interval Cover and Restrained Cover, containing the parameters “number of intervals” and “percent overlap”. All mapper constructions were implemented with percent overlaps ranging from 20% to 80%. The minimum number of intervals applied was three, and the number was increased until constructions were not successful. A ball cover was also used with singular parameter of epsilon. For each set of parameters, ten trials of Mapper constructions were performed, and a successful parameter set was defined as a set that accurately recovered known topological structures at a success rate of 60% or greater.

The trefoil knot is a structure that continuously loops with three major crossings. Thus, the topological data that should be recovered would be closed structures including circles, triangles, hexagons or closed structures containing loops. This construction with a filter that collapses both the x and y coordinates. While performing Mapper constructions on a point data cloud of 240 observations in the shape of a trefoil knot, the following range of parameters were deemed successful in recovering the shape’s topological structure:

####Fixed:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | -                   |   
| 40                | 9-15                |
| 50                | 15-19               |
| 60                | 3     		          |
| 80                | -       			      |

####Restrained:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | 7                   |   
| 40                | 9-11                |
| 50                | 11-25               |
| 60                | 3     		          |
| 80                | 3       			      |

####Ball:
epsilon ranges:            

Meanwhile, the Archimedean spiral is a structure that starts at the origin and wraps around itself continuously, with no intersections. Thus, the topological structure that should be recovered by the Mapper construction should not appear as a closed shape, but instead some type of continuous branching structure, composed of k-simplices where k is > 0.  The Mapper construction parameter ranges below accurately recover the topological structure of a point cloud with 110 observations in the shape of an Archimedean spiral with 1 wrap.

####Fixed:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | 3-5                 |   
| 40                | 3-7                 |
| 50                | 3-13                |
| 60                | -                   |
| 80                | -                   |

####Restrained:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | 3-4                 |   
| 40                | 3-7                 |
| 50                | 3-8                 |
| 60                | -     		          |
| 80                | -       			      |

####Ball:
epsilon ranges: N/A
(0-simplex was produced)            

A planar triangle structure is a filled surface with three edges and three vertices in two dimensional euclidean space. Thus, the topological structure that should be recovered by the Mapper Construction should appear as a closed structure that contains continuous tessellations of triangles. The parameter ranges below were successfully recovered for the Mapper construction of the planar triangle structure with 1250 observations.

####Fixed:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | -                   |   
| 40                | -                   |
| 50                | -                   |
| 60                | 3-30                |
| 80                | 3-40                |

####Restrained:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | -                   |   
| 40                | -                   |
| 50                | -                   |
| 60                | 3-40  		          |
| 80                | 3-50     			      |

####Ball:
epsilon ranges: N/A
(0-simplex was produced)            

Lastly, a figure eight structure is a closed plane curve that crosses itself once and consists of one lobe on each side of the intersection. Thus, the topological structure that should be recovered by the Mapper Construction should appear as a closed structure that contains one central intersection with two closed, symmetrical structures opposite each other. These symmetrical structures on each side of the intersection include triangles, quadrilaterals and curved structures. The parameter ranges below were successfully recovered for the Mapper construction of the figure eight structure with 155 observations.

####Fixed:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | 3-4                 |   
| 40                | 3-7                 |
| 50                | 3-8                 |
| 60                | -                   |
| 80                | -                   |

####Restrained:
| Percent Overlap   | Number of Intervals |          
| ------------------|:-------------------:|
| 20                | 4                   |   
| 40                | 4-6                 |
| 50                | 4-7                 |
| 60                | 7-10  		          |
| 80                | -       			      |

####Ball:
epsilon ranges: N/A
(0-simplex was produced)            
